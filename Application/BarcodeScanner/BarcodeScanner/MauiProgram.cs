﻿using Microsoft.Extensions.Logging;
using ZXing.Net.Maui.Controls;
using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using BarcodeScanner.Interfaces;
using BarcodeScanner.Services;
using BarcodeScanner.Components;
using BarcodeScanner.ViewModels;

namespace BarcodeScanner;

/// <summary>
/// Programme principale de l'application
/// </summary>
public static class MauiProgram
{
    #region Valeurs de classe

    /// <summary>
    /// Contexte de l'API d'Ebay
    /// </summary>
    private static ApiContext _ebayApiContext = new ApiContext();

    #endregion

    #region Méthodes

    /// <summary>
    /// Initialise l'application
    /// </summary>
    /// <returns>L'application configuré</returns>
    public static MauiApp CreateMauiApp()
    {
        ApiContext lApiContext = GetApiContext();

        MauiAppBuilder lBuilder = MauiApp.CreateBuilder();

        lBuilder.UseMauiApp<App>()
            .ConfigureFonts(fonts =>
            {
                fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
            })
            .UseBarcodeReader();

        lBuilder.Services.AddTransient<CameraPage>();
        lBuilder.Services.AddTransient<LibraryPage>();
        lBuilder.Services.AddTransient<MainPage>();
        lBuilder.Services.AddTransient<PricingDisplayPage>();
        lBuilder.Services.AddTransient<SettingsPage>();

        lBuilder.Services.AddSingleton<CameraViewModel>();
        lBuilder.Services.AddSingleton<LibraryViewModel>();
        lBuilder.Services.AddSingleton<PricingDisplayViewModel>();

        lBuilder.Services.AddSingleton<IEbayService, EbayService>();
        lBuilder.Services.AddSingleton<ILibraryService, LibraryService>();
        lBuilder.Services.AddSingleton<IConfigurationService, ConfigurationService>();

#if DEBUG

        lBuilder.Logging.AddDebug();

#endif

        return lBuilder.Build();
    }

    /// <summary>
    /// Récupère le contexte d'API de pour Ebay
    /// </summary>
    /// <returns></returns>
    public static ApiContext GetApiContext()
    {
        if (_ebayApiContext == null)
        {
            _ebayApiContext = new ApiContext();

            _ebayApiContext.SoapApiServerUrl = Preferences.Get("Environment.ApiServerUrl", "default_value");

            ApiCredential lApiCredential = new ApiCredential();

            lApiCredential.eBayToken = Preferences.Get("UserAccount.ApiToken", "default_value");

            _ebayApiContext.ApiCredential = lApiCredential;

            _ebayApiContext.Site = SiteCodeType.Canada;
        }

        return _ebayApiContext;
    }

    #endregion
}
