﻿using BarcodeScanner.Models;

namespace BarcodeScanner.Interfaces;

/// <summary>
/// Interface du service de la bibliothèque de l'utilisateur (économies et produits à reconsulter)
/// </summary>
public interface ILibraryService
{
    #region Gestion des rabais sauvegardés

    /// <summary>
    /// Recharge tous les rabais ayant été effectués
    /// </summary>
    /// <returns>Liste de tous les rabais</returns>
    Task<List<SavedDiscount>> GetAllDiscounts();

    /// <summary>
    /// Recharge tous les rabais ayant été effectuées depuis une certaine date
    /// </summary>
    /// <param name="aDateTime">Date jusqu'à laquelle il faut retourner les rabais</param>
    /// <returns>Liste de rabais</returns>
    Task<List<SavedDiscount>> GetAllDiscountsFromDate(DateTime aDateTime);

    /// <summary>
    /// Sauvegarde une économie réalisée grâce à un prix proposé
    /// </summary>
    /// <param name="aDiscount">Rabais réalisé</param>
    /// <returns>Si réussi</returns>
    Task<bool> PostDiscountInStorage(SavedDiscount aDiscount);

    /// <summary>
    /// Supprime un rabais du stockage de l'application
    /// </summary>
    /// <param name="aSavedDicountId">Identifiant du rabais</param>
    /// <returns>Si réussi</returns>
    Task<bool> DeleteDiscountInStorage(int aSavedDicountId);

    #endregion

    #region Gestions des produits à reconsulter

    /// <summary>
    /// Recharge une liste contenant tous les produits ayant été sauvegardés
    /// </summary>
    /// <returns>Liste de tous les rabais</returns>
    Task<List<Product>> GetAllProducts();

    /// <summary>
    /// Sauvegarde un produit à reconsulter
    /// </summary>
    /// <param name="aProduit">Produit à reconsulter</param>
    /// <returns>Si réussi</returns>
    Task<bool> PostProductInStorage(Product aProduit);

    /// <summary>
    /// Supprime un rabais du stockage de l'application
    /// </summary>
    /// <param name="aProductCode">Code du produit</param>
    /// <returns>Une tâche</returns>
    Task<bool> DeleteProductInStorage(string aProductCode);

    #endregion
}
