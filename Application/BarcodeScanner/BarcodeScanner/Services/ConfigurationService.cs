﻿using BarcodeScanner.Interfaces;

namespace BarcodeScanner.Services;

/// <summary>
/// Service des paramètres servant au fonctionnement de l'application 
/// (réplique ce qu'App.Config fait dans d'autre cadriciel)
/// </summary>
public sealed class ConfigurationService : IConfigurationService
{
    /// <summary>
    /// Clé d'accès à l'API d'Ebay environnement de déploiement (live)
    /// </summary>
    private const string _liveEbayAccessToken = "";

    /// <summary>
    /// Clé d'accès à l'API d'Ebay environnement de développement (simulation/bac à sable)
    /// </summary>
    private const string _devEbayAccessToken = "";

    /// <summary>
    /// Clé d'accès vide
    /// </summary>
    private const string _noTokenAccess = "";
    
    /// <summary>
    /// Retourne la clé d'accès à Ebay
    /// </summary>
    public string LiveAuthEbayAccessToken
    {
        get => Preferences.Get(_liveEbayAccessToken, _noTokenAccess);
    }

    /// <summary>
    /// Retourne la clé d'accès à Ebay
    /// </summary>
    public string DevAuthEbayAccessToken
    {
        get => Preferences.Get(_devEbayAccessToken, _noTokenAccess);
    }
}
