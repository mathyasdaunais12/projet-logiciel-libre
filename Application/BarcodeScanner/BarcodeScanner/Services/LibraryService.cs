﻿using BarcodeScanner.Interfaces;
using BarcodeScanner.Models;
using System;
using System.Text.Json;

namespace BarcodeScanner.Services;

/// <summary>
/// Service de la bibliothèque de l'utilisateur (économies et produits à reconsulter)
/// </summary>
public class LibraryService : ILibraryService
{
    #region Valeurs de classe

    /// <summary>
    /// Chemin complet du stockage des rabais de l'utilisateur
    /// </summary>
    private readonly string _discountStoragePath;

    /// <summary>
    /// Chemin complet du stockage des produits qui pourrons être reconsultés par l'utilisateur
    /// </summary>
    private readonly string _productStoragePath;

    #endregion

    #region Constructeur

    /// <summary>
    /// Constructeur
    /// </summary>
    public LibraryService()
    {
        _discountStoragePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "discounts.json");
        _productStoragePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "products.json");
    }

    #endregion

    #region Gestion des rabais sauvegardés

    public async Task<List<SavedDiscount>> GetAllDiscounts()
    {
        List<SavedDiscount> lDiscounts = new List<SavedDiscount>();
        try
        {
            if (File.Exists(_discountStoragePath))
            {
                using (var lReader = File.OpenText(_discountStoragePath))
                {
                    string? lJson = await lReader.ReadToEndAsync();

                    if (lJson != null)
                    {
                        lDiscounts = JsonSerializer.Deserialize<List<SavedDiscount>>(lJson) ?? new List<SavedDiscount>();
                    }
                }
            }
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.Message);
        }

        return lDiscounts;
    }


    public async Task<List<SavedDiscount>> GetAllDiscountsFromDate(DateTime aDateTime)
    {
        List<SavedDiscount> lDiscounts = new List<SavedDiscount>();
        try
        {
            lDiscounts = await GetAllDiscounts();

            lDiscounts = lDiscounts.Where(lDiscount => lDiscount.DateSaved >= aDateTime).ToList();
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.Message);
        }

        return lDiscounts;
    }


    public async Task<bool> PostDiscountInStorage(SavedDiscount aDiscount)
    {
        bool result = true;
        try
        {
            List<SavedDiscount> lExistingDiscounts = await GetAllDiscounts();

            lExistingDiscounts.Add(aDiscount);

            string? lJson = JsonSerializer.Serialize(aDiscount);

            using (var lWriter = File.CreateText(_discountStoragePath))
            {
                if (lJson != null)
                {
                    await lWriter.WriteAsync(lJson);
                }
            }
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.Message);
            result = false;
        }

        return result;
    }


    public async Task<bool> DeleteDiscountInStorage(int aSavedDicountId)
    {
        bool result = true;
        try
        {
            List<SavedDiscount> lDiscounts = await this.GetAllDiscounts();

            lDiscounts.RemoveAll(lDiscount => lDiscount.Id == aSavedDicountId);

            string? lJson = JsonSerializer.Serialize(lDiscounts);

            using (var lWriter = File.CreateText(_discountStoragePath))
            {
                if (lJson != null)
                {
                    await lWriter.WriteAsync(lJson);
                }
            }
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.Message);
            result = false;
        }

        return result;
    }

    #endregion

    #region Gestions des produits à reconsulter

    public async Task<List<Product>> GetAllProducts()
    {
        List<Product> lProducts = new List<Product>();
        try
        {
            if (File.Exists(_productStoragePath))
            {
                using (var lReader = File.OpenText(_productStoragePath))
                {
                    string? lJson = await lReader.ReadToEndAsync();

                    if (lJson != null)
                    {
                        lProducts = JsonSerializer.Deserialize<List<Product>>(lJson) ?? new List<Product>();
                    }
                }
            }
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.Message);
        }

        return lProducts;
    }


    public async Task<bool> PostProductInStorage(Product aProduit)
    {
        bool result = true;
        try
        {
            List<Product> lExistingProducts = await GetAllProducts();

            lExistingProducts.Add(aProduit);

            string? lJson = JsonSerializer.Serialize(aProduit);

            using (var lWriter = File.CreateText(_productStoragePath))
            {
                if (lJson != null)
                {
                    await lWriter.WriteAsync(lJson);
                }
            }
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.Message);

            result = false;
        }

        return result;
    }


    public async Task<bool> DeleteProductInStorage(string aProductCode)
    {
        bool result = true;
        try
        {
            List<Product> lProducts = await this.GetAllProducts();

            lProducts.RemoveAll(lDiscount => lDiscount.ProductCode == aProductCode);

            string? lJson = JsonSerializer.Serialize(lProducts);

            using (var lWriter = File.CreateText(_productStoragePath))
            {
                if (lJson != null)
                {
                    await lWriter.WriteAsync(lJson);
                }
            }
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.Message);

            result = false;
        }

        return result;
    }

    #endregion
}
