﻿namespace BarcodeScanner;

/// <summary>
/// Classe permettant la navigation dans l'application
/// </summary>
public partial class AppShell : Shell
{
    /// <summary>
    /// Constructeur
    /// </summary>
    public AppShell()
    {
        InitializeComponent();
    }
}
