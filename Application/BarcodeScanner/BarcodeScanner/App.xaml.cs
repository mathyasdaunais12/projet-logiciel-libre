﻿namespace BarcodeScanner;

/// <summary>
/// Classe de l'application
/// </summary>
public partial class App : Application
{
    /// <summary>
    /// Constructeur de l'application
    /// </summary>
    public App()
    {
        InitializeComponent();

        MainPage = new AppShell();

        if (Current != null)
        {
            Current.UserAppTheme = AppTheme.Dark;
        }
    }
}
