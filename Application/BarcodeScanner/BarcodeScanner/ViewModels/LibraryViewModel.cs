﻿using BarcodeScanner.Models;

namespace BarcodeScanner.ViewModels;

/// <summary>
/// Valeurs utilisés par la page de bibliothèque (aussi appelé "Mes ")
/// </summary>
public partial class LibraryViewModel
{
    /// <summary>
    /// Total d'économies effectués par un utilisateur depuis l'installation du logiciel.
    /// </summary>
    public decimal TotalDiscountMadeSinceDownload { get; set; }

    /// <summary>
    /// Liste contenant les produits sauvegardés par l'utilisateur.
    /// </summary>
    public List<Product> ListSavedProducts { get; set; } = null!;

    /// <summary>
    /// Liste contenant la liste d'économies effectués par l'utilisateur.
    /// </summary>
    public List<SavedDiscount> ListSavedDiscounts { get; set; } = null!;
}
