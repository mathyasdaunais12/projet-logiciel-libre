﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace BarcodeScanner.ViewModels;

/// <summary>
/// Valeurs utilisées par la page de la caméra
/// </summary>
public partial class CameraViewModel : ObservableObject
{
    /// <summary>
    /// Code-barres associé au produit
    /// </summary>
    [ObservableProperty]
    public string? productCode;               // SYNTAXE (LOWER CAMEL CASE) OBLIGATOIRE PAR LE DÉCORATEUR

    /// <summary>
    /// Si le bouton de confirmation est activé
    /// </summary>
    [ObservableProperty]
    public bool confirmButtonIsActivated;     // SYNTAXE (LOWER CAMEL CASE) OBLIGATOIRE PAR LE DÉCORATEUR
}
