﻿using BarcodeScanner.Models;

namespace BarcodeScanner.ViewModels;

/// <summary>
/// Valeurs utilisées par la page d'affichage des prix
/// </summary>
public partial class PricingDisplayViewModel
{
    /// <summary>
    /// Code-barres associé au produit
    /// </summary>
    public string ProductCode { get; set; } = null!;

    /// <summary>
    /// Nom donné par l'utilisateur au produit
    /// </summary>
    public string? GivenName { get; set; }

    /// <summary>
    /// Description donnée par l'utilisateur pour le produit
    /// </summary>
    public string? GivenDescription { get; set; }

    /// <summary>
    /// Produit offert par une plateforme
    /// </summary>
    public List<Product> WebStoreListProduct { get; set; } = null!;
}
