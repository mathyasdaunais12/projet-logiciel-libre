using BarcodeScanner.Interfaces;

namespace BarcodeScanner.Components;

/// <summary>
/// Logique des param�tres de l'application
/// </summary>
public partial class SettingsPage : ContentPage
{
    #region Valeurs de classe

    /// <summary>
    /// Interface du service de la biblioth�que de l'utilisateur
    /// </summary>
    private readonly ILibraryService _libraryService;

    /// <summary>
    /// Largeur de l'�cran
    /// </summary>
    private double _screenWidth;

    #endregion

    #region Constructeur

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="aLibraryService">Interface du service de la biblioth�que de l'utilisateur</param>
    public SettingsPage(ILibraryService aLibraryService)
    {
        InitializeComponent();

        _libraryService = aLibraryService;
        _screenWidth = (DeviceDisplay.MainDisplayInfo.Width / DeviceDisplay.MainDisplayInfo.Density);

        SetRadioButtonMargins();
        SetCloseWidthButton();
    }

    #endregion

    #region M�thodes

    /// <summary>
    /// Assigne les marges autour des boutons radios
    /// </summary>
    private void SetRadioButtonMargins()
    {
        double lMarginSize = _screenWidth * 0.35;

        lightThemeButton.Margin = new Thickness(lMarginSize, 50, lMarginSize, 10);
        darkThemeButton.Margin = new Thickness(lMarginSize, 10, lMarginSize, 50);
    }

    /// <summary>
    /// Assigne la largeur du bouton de fermeture (pourcentage %)
    /// </summary>
    private void SetCloseWidthButton()
    {
        Retour.WidthRequest = _screenWidth * 0.25;
    }

    /// <summary>
    /// Assigne le th�me light mode (clair)
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private void LightThemeButtonClicked(object aSender, CheckedChangedEventArgs aArgs)
    {
        if (Application.Current != null)
        {
            Application.Current.UserAppTheme = AppTheme.Light;
        }
    }

    /// <summary>
    /// Assigne le th�me dark mode (sombre)
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private void DarkThemeButtonClicked(object aSender, CheckedChangedEventArgs aArgs)
    {
        if (Application.Current != null)
        {
            Application.Current.UserAppTheme = AppTheme.Dark;
        }
    }

    /// <summary>
    /// Retourne � l'accueil
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private async void SettingsBackButtonClicked(object aSender, EventArgs aArgs)
    {
        await Shell.Current.GoToAsync("///MainPage");
    }

    #endregion
}
