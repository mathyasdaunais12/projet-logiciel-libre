using BarcodeScanner.Interfaces;
using BarcodeScanner.ViewModels;

namespace BarcodeScanner.Components;

/// <summary>
/// Logique de la biblioth�que de produits sauvegard�s et d'�conomies de l'utilisateur
/// </summary>
public partial class LibraryPage : ContentPage
{
    #region Valeurs de classe

    /// <summary>
    /// Interface du service de la biblioth�que de l'utilisateur
    /// </summary>
    private readonly ILibraryService _libraryService;

    /// <summary>
    /// Fournisseur de services et viewModel de l'application
    /// </summary>
    private readonly IServiceProvider _serviceProvider;

    #endregion

    #region Constructeur

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="aLibraryService">Interface du service de la biblioth�que de l'utilisateur</param>
    /// <param name="aServiceProvider">Fournisseur de services et viewModel de l'application</param>
    /// <param name="aLibraryViewModel">Mod�le objet contenant les propri�t�s de la page de librairie</param>
    public LibraryPage(ILibraryService aLibraryService, IServiceProvider aServiceProvider, LibraryViewModel aLibraryViewModel)
    {
        InitializeComponent();

        _libraryService = aLibraryService;

        _serviceProvider = aServiceProvider;

        _ = InitializeViewModel(aLibraryViewModel);
    }

    #endregion

    #region M�thodes

    /// <summary>
    /// S'occupe d'initialiser le ViewModel associ� � la biblioth�que
    /// </summary>
    /// <param name="aLibraryViewModel"></param>
    /// <returns>Une tache asynchrone</returns>
    private async Task InitializeViewModel(LibraryViewModel aLibraryViewModel)
    {
        aLibraryViewModel.ListSavedProducts = await _libraryService.GetAllProducts();

        aLibraryViewModel.ListSavedDiscounts = await _libraryService.GetAllDiscounts();

        aLibraryViewModel.TotalDiscountMadeSinceDownload = 0;

        if (aLibraryViewModel.ListSavedDiscounts.Count() > 0)
        {
            aLibraryViewModel.TotalDiscountMadeSinceDownload = aLibraryViewModel.ListSavedDiscounts
                .Sum(lDiscount => lDiscount.QtySupposedToBeBought * lDiscount.Price);
        }

        BindingContext = aLibraryViewModel;
    }

    /// <summary>
    /// Retourne � l'accueil
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private async void LibraryBackButtonClicked(object aSender, EventArgs aArgs)
    {
        await Shell.Current.GoToAsync("///MainPage");
    }

    /// <summary>
    /// Am�ne l'utilisateur � la page affichant le prix actuel
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private async void ShowCurrentPricingButtonClicked(object aSender, EventArgs aArgs)
    {
        string? lProductCode = GetProductFromParameter(aSender);
        if (lProductCode != null)
        {
            PricingDisplayViewModel lPricingViewModel = _serviceProvider.GetService<PricingDisplayViewModel>()!;

            if (lPricingViewModel != null)
            {
                lPricingViewModel.ProductCode = lProductCode;

                await Shell.Current.GoToAsync("///PricingDisplayPage");
            }
        }
        else
        {
            ThrowError();
        }
    }

    /// <summary>
    /// Supprime un produit de la biblioth�que de l'utilisateur
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private async void DeleteButtonClicked(object aSender, EventArgs aArgs)
    {
        string? lProductCode = GetProductFromParameter(aSender);

        if (lProductCode != null)
        {
            bool isDeleted = await _libraryService.DeleteProductInStorage(lProductCode);

            if (isDeleted)
            {
                LibraryViewModel? lCurrentViewModel = BindingContext as LibraryViewModel;

                if (lCurrentViewModel != null)
                {
                    await InitializeViewModel(lCurrentViewModel);
                }
                else
                {
                    ThrowError();
                }
            }
            else
            {
                ThrowError();
            }
        }
        else
        {
            ThrowError();
        }
    }

    /// <summary>
    /// Retourne le produit envoy� en param�tre et qui est contenu dans un bouton 
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <returns>Un produit si existe</returns>
    private string? GetProductFromParameter(object aSender)
    {
        string? lProduct = "";
        try
        {
            var lButton = (Button)aSender;

            lProduct = lButton.CommandParameter! as string;
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.Message);
        }

        return lProduct;
    }

    /// <summary>
    /// Lance une erreur � l'utilisateur
    /// </summary>
    private async void ThrowError()
    {
        if ((Application.Current != null) && (Application.Current.MainPage != null))
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur innatendue", "Impossible de r�aliser l'action d�sir�e", "OK");
        }
    }

    #endregion
}
