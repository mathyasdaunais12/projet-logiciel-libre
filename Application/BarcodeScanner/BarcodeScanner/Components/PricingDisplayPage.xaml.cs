using BarcodeScanner.Interfaces;
using BarcodeScanner.Models;
using BarcodeScanner.ViewModels;

namespace BarcodeScanner.Components;

/// <summary>
/// Logique de la page contenant les prix des articles en ligne
/// </summary>
public partial class PricingDisplayPage : ContentPage
{
    #region Valeurs de classe

    /// <summary>
    /// Interface du service de gestion d'Ebay
    /// </summary>
    private readonly IEbayService _ebayService;

    /// <summary>
    /// Interface du service de la biblioth�que de l'utilisateur
    /// </summary>
    private readonly ILibraryService _libraryService;

    #endregion

    #region Constructeur

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="aEbayService">Interface du service de gestion d'Ebay</param>
    /// <param name="aLibraryService">Interface du service de la biblioth�que de l'utilisateur</param>
    /// <param name="aViewModel">Mod�le objet utiliser pour afficher des donn�es dans la page</param>
    public PricingDisplayPage(IEbayService aEbayService, ILibraryService aLibraryService, PricingDisplayViewModel aViewModel)
    {
        InitializeComponent();

        _ebayService = aEbayService;
        _libraryService = aLibraryService;

        if (string.IsNullOrEmpty(aViewModel.ProductCode))
        {
            ThrowError();
            GoHome();
        }
        else
        {
            aViewModel.WebStoreListProduct = new List<Product>
            {
                new Product()
                {
                    ProductCode = aViewModel.ProductCode,
                    Name = "Un nom",
                    Description = "Une description",
                    OnlineStoreName = "Une boutique",
                    Price = (decimal) 1.2,
                    PriceWithTaxes = (decimal) 2.5
                }
            };
        }

        BindingContext = aViewModel;
    }

    #endregion

    #region M�thodes

    /// <summary>
    /// Lance une erreur � l'utilisateur
    /// </summary>
    private async void ThrowError()
    {
        if ((Application.Current != null) && (Application.Current.MainPage != null))
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur innatendue", "Veuillez scanner un nouveau code-barres.", "OK");
        }
    }

    /// <summary>
    /// Retourne � l'accueil 
    /// </summary>
    private async void GoHome()
    {
        await Shell.Current.GoToAsync("///MainPage");
    }

    /// <summary>
    /// Converti un type string en entier
    /// </summary>
    /// <param name="aValueToConvert">Valeur � convertir en entier</param>
    private int ParseStringToInt(string aValueToConvert)
    {
        int lResult = 0;
        try
        {
            lResult = int.Parse(aValueToConvert);
        }
        catch (Exception lException)
        {
            Console.WriteLine(lException.ToString());
        }
        return lResult;
    }

    /// <summary>
    /// Affiche le modal permettant de sauvegarder le produit dans la biblioth�que de l'utilisateur
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private void ShowProductSavingModal(object aSender, EventArgs aArgs)
    {
        SaveModal.IsVisible = true;
    }

    /// <summary>
    /// Ferme le modal permettant de sauvegarder le produit dans la biblioth�que de l'utilisateur
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private void HideProductSavingModal(object aSender, EventArgs aArgs)
    {
        SaveModal.IsVisible = false;
    }

    /// <summary>
    /// Sauvegarde le produit ayant �t� scann�
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private async void SaveButtonClicked(object aSender, EventArgs aArgs)
    {
        PricingDisplayViewModel? lViewModel = this.BindingContext as PricingDisplayViewModel;

        if (lViewModel != null && lViewModel.ProductCode != null)
        {
            Product lProductToSave = new Product()
            {
                ProductCode = lViewModel.ProductCode,
                Name = lViewModel.GivenName != null ? lViewModel.GivenName : "",
                Description = lViewModel.GivenDescription != null ? lViewModel.GivenDescription : "",
                OnlineStoreName = "",
                Price = 0,
                PriceWithTaxes = 0
            };

            bool lSucceed = await _libraryService.PostProductInStorage(lProductToSave);

            SuccessContainer.IsVisible = lSucceed;
            ErrorContainer.IsVisible = lSucceed == false;

            SaveModal.IsVisible = false;

            lViewModel.GivenDescription = "";
            lViewModel.GivenName = "";

            BindingContext = lViewModel;
        }
    }

    /// <summary>
    /// Retourne � l'accueil
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private async void PricingBackButtonClicked(object aSender, EventArgs aArgs)
    {
        await Shell.Current.GoToAsync("///MainPage");
    }

    #endregion
}
