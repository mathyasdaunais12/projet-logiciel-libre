using BarcodeScanner.ViewModels;

namespace BarcodeScanner.Components;

/// <summary>
/// Classe contenant les actions de la page de cam�ra.
/// </summary>
public partial class CameraPage : ContentPage
{
    #region Valeurs de classes

    /// <summary>
    /// Fournisseur de services et viewModel de l'application
    /// </summary>
    private readonly IServiceProvider _serviceProvider;

    /// <summary>
    /// Si la lumi�re de la cam�ra est ouverte
    /// </summary>
    private bool _isTorchOn;

    #endregion

    #region Constructeur

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="aServiceProvider">Fournisseur de services et viewModel de l'application</param>
    public CameraPage(IServiceProvider aServiceProvider)
    {
        InitializeComponent();
        ConfigureScanner();

        _isTorchOn = false;

        _serviceProvider = aServiceProvider;

        CameraViewModel lViewModel = new CameraViewModel
        {
            ProductCode = null,
            ConfirmButtonIsActivated = false
        };

        BindingContext = lViewModel;
    }

    #endregion

    #region M�thodes

    /// <summary>
    /// Configure le scanneur de code-barres
    /// </summary>
    public void ConfigureScanner()
    {
        barcodeReader.Options = new ZXing.Net.Maui.BarcodeReaderOptions
        {
            Formats = ZXing.Net.Maui.BarcodeFormat.UpcA | ZXing.Net.Maui.BarcodeFormat.UpcE | ZXing.Net.Maui.BarcodeFormat.Ean13,
            AutoRotate = true,
            Multiple = false,
            TryHarder = true
        };

        barcodeReader.CameraLocation = ZXing.Net.Maui.CameraLocation.Rear;
        barcodeReader.IsTorchOn = _isTorchOn;
    }

    /// <summary>
    /// Appel� lorsqu'un code barre � �t� trouv�
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private void BarcodesDetected(object aSender, ZXing.Net.Maui.BarcodeDetectionEventArgs aArgs)
    {
        var lViewModel = BindingContext as CameraViewModel;
        var lCodebar = aArgs.Results.FirstOrDefault();

        if (lViewModel != null && lCodebar != null)
        {
            lViewModel.ProductCode = lCodebar.Value;
            lViewModel.ConfirmButtonIsActivated = true;
        }
    }

    /// <summary>
    /// D�place l'utilisateur � la page d'affichage des prix.
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private async void ConfirmButtonClicked(object aSender, EventArgs aArgs)
    {
        CameraViewModel? lCameraViewModel = BindingContext as CameraViewModel;

        if ((lCameraViewModel != null) && (lCameraViewModel.ProductCode != null) && (lCameraViewModel.ConfirmButtonIsActivated))
        {
            PricingDisplayViewModel lPricingViewModel = _serviceProvider.GetService<PricingDisplayViewModel>()!;

            if (lPricingViewModel != null)
            {
                lPricingViewModel.ProductCode = lCameraViewModel.ProductCode;

                await Shell.Current.GoToAsync($"///PricingDisplayPage");
            }
        }
        else
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur inattendue", "Veuillez scanner un nouveau code-barres.", "OK");
        }
    }

    /// <summary>
    /// Retourne � l'�cran d'accueil.
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private async void CameraBackButtonClicked(object aSender, EventArgs aArgs)
    {
        await Shell.Current.GoToAsync("///MainPage");
    }

    /// <summary>
    /// Allume ou ferme la lumi�re de la cam�ra. 
    /// </summary>
    /// <param name="aSender">Objet d�clencheur</param>
    /// <param name="aArgs">Informations de l'�v�nement</param>
    private void TurnTorchOnOffButtonClicked(object aSender, EventArgs aArgs)
    {
        barcodeReader.IsTorchOn = !_isTorchOn;
    }

    #endregion
}
