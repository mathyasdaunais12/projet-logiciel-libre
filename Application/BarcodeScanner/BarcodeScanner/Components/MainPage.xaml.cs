﻿namespace BarcodeScanner.Components;

/// <summary>
/// Logique de la page d'accueil
/// </summary>
public partial class MainPage : ContentPage
{
    #region Constructeur

    /// <summary>
    /// Constructeur
    /// </summary>
    public MainPage()
    {
        InitializeComponent();

        CheckInternet();
    }

    #endregion

    #region Méthodes

    /// <summary>
    /// Vérifie si l'internet est disponible sur l'appareil de l'utilisateur
    /// </summary>
    private void CheckInternet()
    {
        bool lHasInternet = Connectivity.Current.NetworkAccess == NetworkAccess.Internet;

        if (lHasInternet == false)
        {
            DisplayAlert("Attention", "Vous n'êtes pas connecté à l'internet. Les produits que vous scannerez pourront seulement être placés dans la bibliothèque.", "Fermer");
        }
    }

    /// <summary>
    /// Dirige l'utilisateur vers la caméra
    /// </summary>
    /// <param name="aSender">Objet déclencheur</param>
    /// <param name="aArgs">Informations de l'événement</param>
    private async void CameraButtonClicked(object aSender, EventArgs aArgs)
    {
        await Shell.Current.GoToAsync("///CameraPage");
    }

    /// <summary>
    /// Dirige l'utilisateur vers sa bibliothèque
    /// </summary>
    /// <param name="aSender">Objet déclencheur</param>
    /// <param name="aArgs">Informations de l'événement</param>
    private async void LibraryButtonClicked(object aSender, EventArgs aArgs)
    {
        await Shell.Current.GoToAsync("///LibraryPage");
    }

    /// <summary>
    /// Dirige l'utilisateur vers la page de paramètres
    /// </summary>
    /// <param name="aSender">Objet déclencheur</param>
    /// <param name="aArgs">Informations de l'événement</param>
    private async void SettingsButtonClicked(object aSender, EventArgs aArgs)
    {
        await Shell.Current.GoToAsync("///SettingsPage");
    }

    #endregion
}
