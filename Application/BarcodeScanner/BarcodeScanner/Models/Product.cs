﻿namespace BarcodeScanner.Models;

/// <summary>
/// Un produit offert par un commercant en ligne
/// </summary>
public class Product
{
    /// <summary>
    /// Code de produit associé à l'article
    /// </summary>
    public string ProductCode { get; set; } = null!;

    /// <summary>
    /// Nom du produit
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// Description du produit
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// Nom du détaillant en ligne offrant ce produit
    /// </summary>
    public string OnlineStoreName { get; set; } = null!;

    /// <summary>
    /// Prix affiché du produit
    /// </summary>
    public decimal Price { get; set; }

    /// <summary>
    /// Prix final avec taxes
    /// </summary>
    public decimal? PriceWithTaxes { get; set; }
}
