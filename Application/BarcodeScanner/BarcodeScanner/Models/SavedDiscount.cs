﻿namespace BarcodeScanner.Models;

/// <summary>
/// Correspond à une économie ayant été réalisé par l'utilisateur
/// </summary>
public class SavedDiscount : Product
{
    /// <summary>
    /// Identifiant du rabais
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Prix auquel l'utilisateur a vu le produit dans en magasin (a été entré manuellement) 
    /// </summary>
    public decimal PriceSeenInStore { get; set; }

    /// <summary>
    /// Prix affiché en ligne sans les frais de transport et les taxes
    /// </summary>
    public decimal PriceOfferedOnlineWithoutFees { get; set; }

    /// <summary>
    /// Quantité de cet article que l'utilisateur ira acheter suite à la proposition de prix
    /// </summary>
    public int QtySupposedToBeBought { get; set; }

    /// <summary>
    /// Date à laquelle le rabais a été effectuée
    /// </summary>
    public DateTime DateSaved { get; set; }
}
