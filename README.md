# BarcodeScanner (projet logiciel libre)

Fondateur initial : Mathyas Daunais

Mis à jour en date du 29 avril 2024



## Description du projet

**À PRENRE EN COMPTE**

L'application est encore en construction. Les fonctionnalités n'ont pas toutes été développées. Pour en connaitre davantage sur l'avancement du projet, veuillez consulter les "Issues" présentement ouverts dans le répertoire GitLab ou vous référer au document "Analyse.md" présent dans ce même répertoire.

**Qu'est-ce que BarcodeScanner?**

BarcodeScanner est un logiciel libre (open source) permettant à ses utilisateurs de scanner des codes de produits de type UpcA, UpcE et Ean13 avec la caméra d'un appareil quelconque. Une fois scannés, les utilisateurs sont en mesure de consulter le prix affichés en ligne, et ce, en dollars canadien.

Un calculateur d'économie permet aux utilisateurs de comparer le prix auquel il a vu le produit en magasin versus le meilleur prix offert en ligne.

Les utilisateurs peuvent sauvegarder des produits et des économies dans leur bibliothèque afin d'être en mesure de reconsulter leur prix et suivre le cumulatif de leurs économies. 

Pour plus d'informations sur les fonctionnalités de l'application, veuillez-vous référer au document "Analyse.md" présent à la racine du répertoire du projet. 



## Plateforme de communication

BarcodeScanner utilise la plateforme de communication Discord, car elle est l'une des plus grandes plateformes de communication de ces dernières années.

Rendez-vous sur le serveur Discord du projet BarcodeScanner pour signaler des problèmes techniques, proposer de nouvelles fonctionnalités et faire part de nouvelles suggestions.

Serveur Discord : https://discord.gg/9RxvbPGa



## Image associée au projet (GitLab, Discord)

L'image ayant été choisi pour représenter le projet BarcodeScanner est une image libre de droit provenant du site Pixabay. L'image a été publié par l'utilisateur 'geralt'. L'image est disponible en suivant le lien suivant: 

Image Pixabay : https://pixabay.com/fr/illustrations/deshabiller-bar-code-code-a-barre-630408/



## Créer et récupérer une copie du projet (fork)

**(1) Créer un "fork" du projet.**

Dans le menu principal du répertoire du projet, déplacez-vous en haut à droite et cliquez sur le bouton "fork". 

Remplissez les champs du formulaire selon vos spécifications.

****

**(2) Clônez votre répertoire sur votre machine** 

Dans l'interface GitLab web, déplacez-vous dans le répertoire de votre fork.

Afin de récupérer le lien de votre répertoire, en haut à droite de la page principale de votre répertoire, cliquez sur l'option "Code".

Dans le petit menu qui s'ouvre, cliquez sur l'option "Clone with HTTPS" pour copier l'URL.

Ouvrez votre explorateur de fichier.

Naviguez vers l'emplacement où vous désirez cloner le projet.

À cet endroit, ouvrez un invite de commande (cmd) et entrez la commande suivante :

```bash
git clone <mettre_url_de_votre_fork_sans_crochets>
```





## Démarrer le projet

### (1)  Restaurer les "packages" utilisés par le cadriciel .NET Maui.

Ouvrez votre explorateur de fichier.

Dirigez-vous à l'emplacement où vous avez "fork" le projet.

À cet endroit, ouvrez un invite de commande (cmd), puis déplacez-vous dans le fichier suivant :

```bash
cd Application\BarcodeScanner\BarcodeScanner
```

Afin de restaurer les "packages", utilisez la commande suivante :

```bash
dotnet restore
```

****

### (2)  Ouverture de la solution dans l'éditeur de code

**_Note : Il est possible d'ouvrir le projet en double cliquant sur le fichier BarcodeScanner.sln directement dans l'explorateur de fichier._**

Ouvrez votre explorateur de fichier.

Dirigez-vous à l'emplacement ou vous avez "fork" le projet.

À cet endroit, ouvrez un invite de commande (cmd), puis déplacez-vous dans le fichier suivant :

```bash
cd Application\BarcodeScanner
```

Une fois déplacé, démarez le fichier BarcodeScanner.sln. avec la commande suivante.

```bash
dotnet build BarcodeScanner.sln
```

Un éditeur de code comme Visual Studio 2022 dervait s'ouvrir, vous n'avez plus qu'à appuyer sur la bouton de démarrage "Start" pour lancer le projet

**_Note : Il est possible qu'aucune machine ne soit configurer pour démarrer le projet. Si le cas s'applique, veuillez suivre l'étape 3._**

****

### (3)  Configuration d'une machine virtuelle (ex: Android) dans Visual Studio 2022

Ouvrez Visual Studio 2022

Dans la barre du haut, sélectionnez le menu "Outils"

Dans le menu déroulant, naviguer au dessus de l'option "Android" et sélectionnez le sous-option "Gestionnaire d'appareils Android..."

Appuyez sur le bouton "+Nouveauté" présent dans le coin en haut à droite du gestionnaire d'appareils Android.

La portion de gauche permet de sélectionner l'émulateur à créer et son nom. La portion du centre n'est pas pertinent dans notre cas.

Une fois le formulaire rempli avec la machine à créer, appuyez sur le bouton "Créer" dans le coin en bas à droite.

Attendez que l'installation s'effectue, il se peut que vous aillez à redémarrer l'éditeur Visual Studio 2022.

Une fois terminé, cliquez sur la petite flèche qui pointe vers le bas, située à droite du bouton de démarrage.

Dans le menu déroulant, sélectionnez l'option "Émulateurs Android" puis le nom de votre émulateur comme sous option. 



## Mettre en place les runners associés au Pipeline de GitLab

Le Pipeline CI/CD du projet GitLab utilise des runners afin de vérifier si le projet est en mesure d'être compilé sur toutes les plateformes nécéssaires. Il permet également d'exécuter des tests si des tests se voit être éventuellement créés.

Voici les noms (tags) de ces runners ainsi que l'opérateur de système auquelles elles sont liées et l'image qu'elles utilisent : 

- dotnet/sdk:8.0-windows          --->   Runner : Windows   --->   Image :  mcr.microsoft.com/dotnet/sdk:8.0
- dotnet/sdk:8.0-android            --->   Runner : Linux        --->   Image :  mcr.microsoft.com/dotnet/sdk:8.0
- dotnet/sdk:8.0-ios                   --->   Runner : IOS           --->   Image :  mcr.microsoft.com/dotnet/sdk:8.0
- dotnet/sdk:8.0-maccatalyst    --->   Runner : IOS            --->   Image : mcr.microsoft.com/dotnet/sdk:8.0

Ces runners sont utiliser localement avec Docker. Voici les étapes permettant de créer les runners.

****

**(1) Installer Docker localement (s'il le faut)**

Installez la version de docker appropriée pour votre opérateur de système en suivant le lien suivant https://www.docker.com/products/docker-desktop/

Une fois téléchargé, exécutez le fichier .exe et complétez les étapes de l'exécutable.

****

**(2) Création d'un runner dans le répertoire GitLab**

Dans le répertoire ou il faut créer les runners, naviguer dans Settings puis dans CI/CD.

Trouvez l'onglet intitulé "Runner" puis cliquez sur "Expand".

Appuyez sur "New projet runner".

Sélectionnez seulement l'opérateur de système désirée.

Ajouter un tag (référez-vous aux noms de tag ci-haut).

Cliquez sur "Create runner"

****

**(3) Installation de GitLab Runner localement (s'il le faut)**

Ouvrez powershell afin de créer un dossier permettant d'accueillir la configuration de GitLab runner, puis déplacez-vous dans celui-ci

```powershell
New-Item -Path 'C:\GitLab-Runner' -ItemType Directory
cd 'C:\GitLab-Runner'
```

Déplacez-vous dans l'interface web de GitLab afin de récupérer le jeton d'enregistrement du runner. 

Copier les lignes d'enregistrement du runner et inscrivez les dans la fenêtre powershell que vous avez d'ouvert.

```powershell
.\gitlab-runner.exe register  --url https://gitlab.com  --token votre_jeton
```

Remplacez "votre_jeton" par votre jeton. Plusieurs questions apparaiteront, veuillez y répondre de cette façon : 

- Appuyez sur la touche Entrée pour la question "Enter the GitLab instance URL... [https://gitlab.com]"
- Entrez le nom que vous souhaitez donner à votre runner localement (peut-être différent du nom du runner GitLab).
- Pour l'exécuteur, entrez la réponse suivante : docker
- Dans notre cas, tous les runners possèdent des images : mcr.microsoft.com/dotnet/sdk:8.0

Pour finaliser la configuration, roulez les commandes suivantes dans l'invite powershell

```powershell
.\gitlab-runner.exe install
.\gitlab-runner.exe start
```

Votre runner sera enfin près à être utilisé.

**_Note : Si vous compter créer d'autres runners, utilisez ".\gitlab-runner.exe stop" pour arrêter le processus actuel. De plus, vous n'aurez pas besoin de resaisir la commande .\gitlab-runner.exe install_**



## Licence

Licence MIT (FR)

Copyright (c) 2024 - Mathyas Daunais

La permission est accordée, gratuitement, à toute personne qui obtient une copie de ce logiciel et des fichiers de documentation associés au logiciel BarcodeScanner, de traiter dans le logiciel sans restriction, y compris sans limitation les droits à utiliser, copier, modifier, fusionner, publier, distribuer, sous-licencier et/ou vendre des copies du logiciel, et à permettre aux personnes à qui le logiciel est fourni de le faire, sous réserve des conditions suivantes :

L'avis de droit d'auteur ci-dessus et cet avis de permission doivent être inclus dans tous les copies ou parties substantielles du Logiciel.

LE LOGICIEL EST FOURNI "TEL QUEL", SANS GARANTIE D'AUCUNE SORTE, EXPRESSE OU IMPLICITE, Y COMPRIS, MAIS SANS S'Y LIMITER, LES GARANTIES DE COMMERCIALITÉ, ADAPTATION À UN USAGE PARTICULIER ET ABSENCE DE CONTREFAÇON. EN AUCUN CAS LES AUTEURS OU DÉTENTEURS DU DROIT D'AUTEUR NE PEUVENT ÊTRE TENUS RESPONSABLES POUR TOUTE RÉCLAMATION, DOMMAGES OU AUTRES RESPONSABILITÉ, QUE CE SOIT DANS UNE ACTION CONTRACTUELLE, UN DÉLIT OU AUTRE, DÉCOULANT DE, HORS DE OU EN LIEN AVEC LE LOGICIEL OU L'UTILISATION OU AUTRES TRANSACTIONS DANS LE LOGICIEL.





