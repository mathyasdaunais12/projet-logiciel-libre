# Analyse du projet

## Lien du répertoire GitLab

https://gitlab.com/mathyasdaunais12/projet-logiciel-libre



## Dépendances du projet (packages ajoutés)

- ZXing.Net.Maui.Controls
  - Permet de lire les codes barres avec la caméra d'un appareil (retourne une série de chiffres correspondant au code du produit)
  - Documentation de la librairie : https://github.com/Redth/ZXing.Net.Maui?tab=readme-ov-file
- eBay
  - Afin d'utiliser Finding API de eBay
  - Permet de chercher un produit à l'aide du numéro obtenu par le code barre.
  - Types de code de produit possibles de chercher UPC, ISBN, EAN.
  - Documentation de la librairie : https://developer.ebay.com/api-docs/user-guides/static/finding-user-guide/finding-searching-by-product.html

- Community.ToolKit.Mvvm
  - Librairie d'outils "helpers"
  - Permet une meilleure interaction entre les vues (.xaml) et les viewModels à l'aide de mécanisme d'observable.



## Standards de programmation

### Standards propre à l'application

- Précéder les paramètres ayant été reçus par les méthodes par un "a" minuscule (signifie argument).

- Précéder les valeurs de champ privé des classes par une barre en bas "_".

- Inscrire les éléments de code de programmation en anglais, incluant les noms de fichiers.

- Inscrire la documentation du code en français.

- Nommer les variables de façon significative.

- Utiliser des interfaces pour les fichiers de services.

- Regrouper les fichiers dans des dossiers (ex: Models, ViewModels, Services, Interfaces, Composantes...) afin d'améliorer la lisibilité. 

- Les composants visuels représentant des pages (pages XAML) doivent être nommées à l'aide du mot « Page » à la fin (ex: MainPage.xaml). 

  **(Note : les fichiers App.xaml et AppShell.xaml ne doivent pas posséder le mot « Page », puisqu'ils sont des fichiers de base du cadriciel.)**

  

### Standards du cadriciel .NET Maui

- L'application doit utiliser une structure MVVM (Model-View-ViewModel). 

- L'interface utilisateur doit être conçue à l'aide du langage de balisage XAML.

- La logique d'application doit être conçu à l'aide du langage C#.

- Les permissions doivent être inscrits dans les fichiers de permissions nécessaires pour les plateformes Android, IOS, Mac et Windows.

  

### Standards  du langage C#

- Inscrire les noms de fichiers et de classes en utilisant la convention de nommage PascalCase.
- Inscrire les noms de variables en utilisant la convention de nommage camelCase.
- Inscrire les accolades {  } en dessous des lignes de code.
- Documenter le code afin d'expliquer ce que les méthodes/procédures ont pour but de réaliser.
- Si possible, initialiser toutes les valeurs de classe dans le constructeur.
- Utiliser des régions afin de séparer le code contenu dans les classes.
- Utiliser la propriété "readonly" lorsqu'une valeur est définie et qu'elle ne changera jamais.
- Les exceptions doivent être gérées dans les situations où elles peuvent survenir.



## Fonctionnalités de l'application (user stories)

- Scanner des codes barres de produits à l'aide de la caméra de l'appareil.
- Changer le thème de l'application (sombre/claire) directement dans l'application
- Allumer et éteindre la torche de la caméra (lumière) à l'aide d'un bouton dans l'interface.
- Ajouter un code de produit à la bibliothèque.
- Ajouter un nom et une description afin de décrire le code de produit sauvegardé dans la bibliothèque.
- Consulter un cumulatif d'économies effectuées grâce à l'application depuis l'installation.
- Charger un code de produit ayant été sauvegardé dans la bibliothèque de l'utilisateur afin de le reconsulter.
- Supprimer un produit sauvegardé dans la bibliothèque.
- **(Non développé)** Entrer le prix auquel l'utilisateur a vu le produit en magasin. Ce champ doit être affiché sur la page affichant les prix en ligne.
- **(Non développé)** Obtenir le prix affiché sur une plateforme de commerce en ligne (comme EBay), et ce, sans inclure les frais de transport et ni les taxes (en dollars canadien).
- **(Non développé)** Configurer un calculateur d'économies afin de permettre à l'utilisateur de consulter une économie possible de réaliser grâce aux produits en ligne (en se référant au prix saisi dans le champ texte).
- **(Non développé)** Entrer la quantité qui compte être achetée par l'utilisateur par rapport à un des prix affiché en ligne.
- **(Non développé)** Ajouter l'économie total (en se référant au calculateur d'économies) à la banque d'économies ayant été réalisées depuis l'installation de l'application.



## Stockage de données

Les données de l'application seront stockées dans l'appareil de l'utilisateur.



## Permissions de l'appareil

- Accès à l'internet
- Accès à la caméra
- Accès au stockage



## Diagramme

### Diagramme de classes

- Voir le fichier situé à l'emplacement "Diagramme\Diagramme_de_classe.drawio" afin de modifier le diagramme de classe. 
- Voir l'image situé à l'emplacement "Diagramme\Diagramme_de_classe.png" afin de consulter visuellement le diagramme de classe.



