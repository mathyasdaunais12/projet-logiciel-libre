## Description

(Décrire le problème rencontré)


## Étapes pour reproduire

(Comment reproduire le problème (étapes, configurations, séquence, etc.)


## Quel est le comportement actuel?

(Ce qui se produit lorsque vous effectuez les étapes)


## Quel est le comportement attendu?

(Ce qui devrait se produire ou ce que vous devriez voir)


## Captures d'écran et logs pertinents

(Ajoutez le ou les logs pertinents - utiliser les blocs de code markdown (```) pour formater le log ou le code.



## Solutions possibles



/label ~Bug ~Open/Ouvert
