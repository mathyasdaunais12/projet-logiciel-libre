# License / Licence

MIT License (EN)

Copyright (c) 2024 - Mathyas Daunais

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files associated to BarcodeScanner software, to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions : 

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

\----------------

Licence MIT (FR)

Copyright (c) 2024 - Mathyas Daunais

La permission est accordée, gratuitement, à toute personne qui obtient une copie de ce logiciel et des fichiers de documentation associés au logiciel BarcodeScanner, de traiter dans le logiciel sans restriction, y compris sans limitation les droits à utiliser, copier, modifier, fusionner, publier, distribuer, sous-licencier et/ou vendre des copies du logiciel, et à permettre aux personnes à qui le logiciel est fourni de le faire, sous réserve des conditions suivantes :

L'avis de droit d'auteur ci-dessus et cet avis de permission doivent être inclus dans tous les copies ou parties substantielles du Logiciel.

LE LOGICIEL EST FOURNI "TEL QUEL", SANS GARANTIE D'AUCUNE SORTE, EXPRESSE OU IMPLICITE, Y COMPRIS, MAIS SANS S'Y LIMITER, LES GARANTIES DE COMMERCIALITÉ, ADAPTATION À UN USAGE PARTICULIER ET ABSENCE DE CONTREFAÇON. EN AUCUN CAS LES AUTEURS OU DÉTENTEURS DU DROIT D'AUTEUR NE PEUVENT ÊTRE TENUS RESPONSABLES POUR TOUTE RÉCLAMATION, DOMMAGES OU AUTRES RESPONSABILITÉ, QUE CE SOIT DANS UNE ACTION CONTRACTUELLE, UN DÉLIT OU AUTRE, DÉCOULANT DE, HORS DE OU EN LIEN AVEC LE LOGICIEL OU L'UTILISATION OU AUTRES TRANSACTIONS DANS LE LOGICIEL.