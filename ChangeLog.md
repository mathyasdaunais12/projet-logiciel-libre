# ChangeLog



## v.0.0

Aucune mises à jour n'est pertinente pour le moment

- Ajout du scannage des codes barres de produits à l'aide de la caméra de l'appareil.

- Ajout du changement de thème de l'application (sombre/claire) directement dans l'application

- Ajout de la possibilité d'allumer et d'éteindre la torche de la caméra (lumière) à l'aide d'un bouton dans l'interface.

- Ajout de la possibilité d'ajouter un code de produit à la bibliothèque.

- Ajout de la possibilité d'ajouter un nom afin de décrire le code de produit sauvegardé dans la bibliothèque.

- Ajout de la possibilité de consulter un cumulatif d'économies effectuées grâce à l'application depuis l'installation.

- Ajout de la possibilité charger un code de produit ayant été sauvegardé dans la bibliothèque de l'utilisateur afin de le reconsulter.

- Ajout de la possibilité d'entrer le prix auquel l'utilisateur a vu le produit en magasin. Ce champ doit être affiché sur la page affichant les prix en ligne.

  

## v1.0

À venir...